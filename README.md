Feature Counter
===========

ImageJ macros to help identify tumour-like areas of an image

Installation
----

This works with ImageJ (at least version 1.53k), which can be obtained from here:

https://imagej.net/ij/download.html

FIJI has a different plugin structure, and may not work with Feature Counter. The version on my Mac computer won't allow me to install plugins.

You need to open up the ImageJ image (right click, show package contents), then add 'Autoscale.ijm', 'SubtractBlue.ijm' and 'MeasureFeatures.ijm' into the 'plugins' directory.

After installing and restarting ImageJ, the feature counter script can be run via Plugins -> MeasureFeatures.
